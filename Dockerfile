FROM archlinux:base-devel

LABEL org.opencontainers.image.created="%%CREATED%%"
LABEL org.opencontainers.image.authors="Ricardo Band <email@ricardo.band>"
LABEL org.opencontainers.image.url="https://gitlab.com/XenGi0/containers/archlinux-aur"
LABEL org.opencontainers.image.source="https://gitlab.com/XenGi0/containers/archlinux-aur"
LABEL org.opencontainers.image.version="latest"
LABEL org.opencontainers.image.revision="%%COMMIT%%"
LABEL org.opencontainers.image.title="archlinux-aur"
LABEL org.opencontainers.image.description="archlinux:base-devel with aur helper (yay) preinstalled"


RUN pacman -Syyu --noconfirm --noprogressbar sudo git go pacman-contrib \
 && useradd --uid 1000 -c "aur helper" -m -s /bin/sh dev \
 && echo "dev ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

USER 1000

RUN git clone https://aur.archlinux.org/yay.git $HOME/yay \
 && cd $HOME/yay; makepkg -csi --noconfirm --noprogressbar \
 && sudo pacman -Rs --noconfirm --noprogressbar go \
 && rm -rf $HOME/yay \
 && rm -rf $HOME/.cache \
 && sudo rm -rf /var/cache/pacman/pkg/*
